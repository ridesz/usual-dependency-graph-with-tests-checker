# @ridesz/usual-dependency-graph-with-tests-checker

The typical dependency graph with test checking config I use for my TypeScript projects.

## How to use

### `package.json` setup

Just please add this `scripts` tag part to your `package.json` file too:

```json
{
    "test:dependency:test": "npx @ridesz/usual-dependency-graph-with-tests-checker",
}
```

### GitLab CI config

Please add the necessary parts to your `.gitlab-ci.yml` file:

```yml
image: node:latest

stages:
    - test

dependecy:
    stage: test
    artifacts:
        paths:
            - build/
        expire_in: 1 week
        when: always
    script:
        - npm install
        - npm rebuild
        - npm run test:dependency:test
```

This would use the latest node image and do the dependency graph check as part of the `test` stage.

### Running manually


If you would like to run the documentation generation manually then you can use the following command:

```bash
npm run test:dependency
```

## Some implementation details

The result of the check (the graph in SVG and PNG formats) is generated into the `reports/dependencyWithTests` folder.

The checker only checks the sources from the `src` and the `tests` folders.
