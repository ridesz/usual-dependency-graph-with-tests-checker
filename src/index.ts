#!/usr/bin/env node

import * as shellJs from "shelljs";
import * as fs from "fs-extra";
import sharp from "sharp";
import { graphviz } from "@hpcc-js/wasm";
import * as tmp from "tmp";
import dcJson from "./dependency-cruiser.json";
import path from "path";

const outputFolder = "reports/dependencyWithTests";

function createJson(): string {
    const dir = tmp.dirSync();
    const path = `${dir.name}/dc.json`;

    // Override some parts of the config JSON
    dcJson.options.tsConfig.fileName = `${projectFolder}/tsconfig.json`;
    dcJson.options.exclude = "(^node_modules)";
    fs.writeFileSync(path, JSON.stringify(dcJson));
    return path;
}

function getProjectFolder(): string {
    const result = shellJs.pwd();
    if (result.code !== 0) {
        throw new Error("Can't get the project folder path");
    }
    return result.stdout;
}

const npxFolder =
    process.env.CI_PROJECT_URL !== undefined
        ? path.normalize(`${__dirname}../../../../../../`)
        : path.normalize(`${__dirname}../../../`);

const projectFolder = getProjectFolder();
const jsonPath = createJson();

async function removingOldFolder(): Promise<void> {
    const result = shellJs.exec(`npx rimraf ${projectFolder}/${outputFolder}`);
    if (result.code !== 0) {
        throw new Error("Can't remove the old dependency folder.");
    }
    fs.ensureDirSync(`${projectFolder}/${outputFolder}`);
}

function fixingSvgHrefs(svg: string): string {
    const projectUrl = process.env.CI_PROJECT_URL;
    if (projectUrl !== undefined) {
        // GitLab CI
        return svg.replace(new RegExp('href="src', "g"), 'href="' + projectUrl + "/tree/master/src");
    } else {
        // Not GitLab CI
        return svg.replace(new RegExp('href="src', "g"), 'href="../../src');
    }
}

function runDependencyCruiser(isDot: boolean): shellJs.ShellString {
    const parts: string[] = [];
    parts.push(`${npxFolder}/node_modules/dependency-cruiser/bin/dependency-cruise.js`);
    parts.push(`--validate ${jsonPath}`);
    parts.push(`${projectFolder}/src`);
    parts.push(`${projectFolder}/tests`);
    if (isDot) {
        parts.push(`--output-type dot`);
    }
    const result = shellJs.exec(parts.join(" "), {
        silent: true,
    });

    if (result.code !== 0) {
        throw new Error("Dependency cruiser error");
    }
    return result;
}

function generateDot(): string {
    return runDependencyCruiser(true).stdout;
}

async function generateSvgFromDot(dot: string): Promise<string> {
    const svg = await graphviz.layout(dot, "svg", "dot");
    return fixingSvgHrefs(svg);
}

async function renderImages(): Promise<void> {
    const dot = generateDot();

    const svg = await generateSvgFromDot(dot);

    // Save SVG
    fs.writeFileSync(`${projectFolder}/${outputFolder}/dependency.svg`, svg);
    fs.writeFileSync(
        `${projectFolder}/${outputFolder}/dependency.html`,
        "<html><head><title>Dependency graph</title></head><body>" + svg + "</body></html>",
    );

    // Save PNG
    await sharp(Buffer.from(svg)).png().toFile(`${projectFolder}/${outputFolder}/dependency.png`);
}

function checkDependencies(): void {
    if (runDependencyCruiser(false).grep("✖").stdout.trim() !== "") {
        throw new Error("Error cross detected! (Dependency cruiser error.)");
    }
}

async function main(): Promise<void> {
    try {
        console.log("Generating dependency graphs");

        await removingOldFolder();

        await renderImages();

        checkDependencies();

        console.log();
        console.log("Dependency graph generation finished.");
        console.log();
        shellJs.exit(0);
    } catch (error) {
        console.log();
        console.log(`ERROR WITH DEPENDENCY GRAPH GENERATION: ${(error as Error).message}`);
        console.log();
        shellJs.exit(1);
    }
}

main();
